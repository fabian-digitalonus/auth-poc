var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var flash = require('connect-flash');
var session = require('express-session');
var passport = require('passport');
var mongoose = require('mongoose');
var config = require('config');

var app = express();

mongoose.connect(config.get('datasource.url') + ':' + config.get('datasource.port'));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

require('./config/passport')(passport);

// Middleware
app.use(logger('dev'));
app.use(cookieParser());
app.use(bodyParser());
app.use(session({
    name: 'session',
    secret: config.get('session.secret')
}));
app.use(passport.initialize());
app.use(passport.session());
// Add passport remember-me.
app.use(flash());

app.use(express.static(path.join(__dirname, 'public/assets')));

// Filter chain
require('./app/filter-chain.js')(app, passport);

// Routes
require('./app/routes.js')(app, passport);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

module.exports = app;
