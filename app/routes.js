module.exports = function (app, passport) {
    //Routes
    var index = require('./routes/index'),
        users = require('./routes/users'),
        auth  = require('./routes/auth')(passport);

    app.use('/', index);
    app.use('/auth', auth);
    app.use('/users', users);
};