var express = require('express'),
    router = express.Router(),
    User = require('../models/user');

module.exports = function (passport) {
    router.get('/signup', function(req, res, next) {
        res.render('signup', {message: req.flash('signupMessage')});
    });

    router.post('/signup', function(req, res, next) {
        // find a user whose email is the same as the forms email
        // we are checking to see if the user trying to login already exists
        User.findOne({'local.email': req.body.email}, function (err, user) {
            // if there are any errors, return the error
            if (err)
                return next(err);

            // check to see if theres already a user with that email
            if (user) {
                req.flash('signupMessage', 'That email is already taken.');
                return res.redirect('signup');
            } else {
                // if there is no user with that email
                // create the user
                var newUser = new User();

                // set the user's local credentials
                newUser.local.email = req.body.email;
                newUser.local.first_name = req.body.first_name;
                newUser.local.last_name = req.body.last_name;
                newUser.local.nickname = req.body.nickname;
                newUser.local.password = newUser.generateHash(req.body.password);

                // save the user
                newUser.save(function (err, user) {
                    if (err) next(err);
                    req.flash('signupMessage', 'New user has been created.');                        
                    req.login(user, function(err) {
                        if (err) { 
                            return next(err);
                        }
                        return res.redirect('/profile');
                    });
                });
            }
        });
    });

    router.get('/login', function(req, res, next) {
        res.render('login', {message: req.flash('loginMessage')});
    });

    router.post('/login', passport.authenticate('local', {
            successRedirect: '/profile', 
            failureRedirect: '/auth/login', 
            failureFlash: true,
            passReqToCallback: true
    }));
    
    router.post('/logout', function(req, res, next) {
        req.logout();
        res.redirect('/');
    });
    
    return router;
};

