var express = require('express'),
    router = express.Router(),
    isAuthenticated = require('../helpers/is-authenticated');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.redirect('/profile')
});

router.get('/profile', isAuthenticated, function(req, res, next) {
  res.render('profile', { user: req.user });
});

module.exports = router;
