mockScribe = require('../../test/mocks/get-scribe');

/*
What comes first? the reading of the scribe object or the deserialization of the user?
Do we need to derive one from the other?

If we continue to use 'session' as the cookie for storing the scribe object then we would 
need another cookie for the passport session where we can save the id of the user for 
serialization/deserialization. We could also store the id of the scribe object as another
key-value pair in the passport session but that would be different from the current 'session'
cookie.
*/

function filter() {
    return function(req, res, next) {

        // Are we still reading the below value from the session cookie or do we encrypt it as
        // part of the passport session?
        console.log('=== Start scribe reader filter ===');
        var scribeId = '1&662A0BF757394B65BB04D4DAD79CF11F';
        // Get the scribe object and add it to the request.
        req.scribe = mockScribe();
        console.log('==== End scribe reader filter ====');
        next();
    };
}

module.exports = filter;

