module.exports = function (app, passport) {
    // Filters
    var globalId = require('./filters/global-id'),
        salesProgramId = require('./filters/sales-program-id'),
        scribe = require('./filters/scribe-reader'),
        jwt = require('./filters/jwt');

    app.use(globalId());
    app.use(salesProgramId());
    app.use(scribe());
    app.use(jwt());
};